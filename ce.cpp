/* SPDX-License-Identifier: LGPL-3.0-or-later */
/* Copyright © 2022 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
 *                  Matthias Kretz <m.kretz@gsi.de>
 */
#include "graph.h"
#include <array>

int test0()
{
  auto merged
    = gsi::graph::merge<0, 0>(gsi::graph::scale<int, -1>(),
                              gsi::graph::merge<0, 0>(gsi::graph::scale<int, 2>(),
                                                      gsi::graph::adder<int>()));
  std::array<int, 4> a = {1, 2, 3, 4};
  std::array<int, 4> b = {10, 10, 10, 10};

  int r = 0;
  for (int i = 0; i < 4; ++i)
    r += merged.process_one(a[i], b[i]);
  return r;
}

int test1(gsi::graph::port_data<int, 1024>& result,
          const std::vector<int> a, const std::vector<int> b)
{
  auto merged
    = gsi::graph::merge<0, 0>(gsi::graph::scale<int, -1>(),
                              gsi::graph::merge<0, 0>(gsi::graph::scale<int, 2>(),
                                                      gsi::graph::adder<int>()));
  merged.process_batch(result, std::span(a), std::span(b));
  auto reader = result.request_read();
  int r = 0;
  for (auto z : reader)
    r += z;
  return r;
}
